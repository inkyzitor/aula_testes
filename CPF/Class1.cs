﻿using System.Text.RegularExpressions;
using System.Numerics;
using System.Linq;
using System;

namespace CPF
{
    public class Class1
    {

        public static string regexCode = "^\\d{3}\\.?\\d{3}\\.?\\d{3}\\-?\\d{2}$";
        public static string remocao = @"[\\.]+|[-]";

        

        public bool validarTamannho(string aindaNada)
        {
            return aindaNada.Length == 11;

        }


        public int[] InteiroValido(string valorInt)
        {
            
            return valorInt.Select(c => Int32.Parse(c.ToString())).ToArray();
               
        }

        public int[] multiplicarValoresDigito1(int[] aindaNada)
        {
            int [] CadeiaMultitplicadora1 = new int [] {10,9,8,7,6,5,4,3,2};
            int [] aindaNadaArray = new int [9];

            for(int c=0; c<CadeiaMultitplicadora1.Length; c++)
            {
                aindaNadaArray[c] = CadeiaMultitplicadora1[c] * aindaNada[c];
            }
            return aindaNadaArray;
        }
        public int restoMultiplicacaoDigito1(int aindaNada)
        {
            int [] resultadoCadeia1 = new int [] {10,9,8,28,24,20,28,21,14};
            int somaDeTudo =0;
            int resto =0;
            
            for(int s=0; s<resultadoCadeia1.Length; s++)
            {
                somaDeTudo = somaDeTudo + resultadoCadeia1[s];
            }
            resto = somaDeTudo % 11;
            if(resto < 2)
            {
                resto = 0;
            }
            else
            {
                resto = 11 - resto;
            }
            return resto;

        }

        public int[] multiplicarValoresDigito2(int[] aindaNada)
        {
            int [] CadeiaMultitplicadora2 = new int [] {11,10,9,8,7,6,5,4,3,2};
            int [] aindaNadaArray = new int [10];

            for(int c=0; c<CadeiaMultitplicadora2.Length; c++)
            {
                aindaNadaArray[c] = CadeiaMultitplicadora2[c] * aindaNada[c];
            }
            return aindaNadaArray;
        }
        public int restoMultiplicacaoDigito2(int aindaNada)
        {
            int [] resultadoCadeia2 = new int [] {11,10,9,32,28,24,35,28,21,6};
            int somaDeTudo =0;
            int resto =0;
            
            for(int s=0; s<resultadoCadeia2.Length; s++)
            {
                somaDeTudo = somaDeTudo + resultadoCadeia2[s];
            }
            resto = somaDeTudo % 11;
            if(resto < 2)
            {
                resto = 0;
            }
            else
            {
                resto = 11 - resto;
            }
            return resto;

        }

        public string validarTudo(string aindaNada)
        {
            string stringValida = ("111.444.777-35");
            string resultado = Regex.Replace(stringValida,remocao, "");
            return resultado;
            
        }

        
    }
}
