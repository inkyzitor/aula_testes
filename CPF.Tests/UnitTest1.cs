using System.Reflection;
using System;
using Xunit;

namespace CPF.Tests
{
    public class UnitTest1
    {
        [Theory]
        [InlineData("aaaaaaaaadf")]
        [InlineData("11111111123")]
        [InlineData("-----------")]
        public void TestDetamanhoValido(string aindaNada)
        {
            Class1 cpf = new Class1();
            bool retorno = cpf.validarTamannho(aindaNada);
            Assert.True(retorno);
        }

        [Theory]
        [InlineData("aaaaaaa")]
        [InlineData("111111111234")]
        [InlineData("-----------ee")]
        public void TestDetamanhoInvalido(string aindaNada)
        {
            Class1 cpf = new Class1();
            bool retorno2 = cpf.validarTamannho(aindaNada);
            Assert.False(retorno2);
        }
           
        [Theory]
        [InlineData("11111111111", new [] {1,1,1,1,1,1,1,1,1,1,1})]
        public void TestarInteiroValido(string aindaNada,int [] valorInt)
        {
            Class1 cpf = new Class1();
            int [] retorno = cpf.InteiroValido(aindaNada);
            Assert.Equal(valorInt,retorno);
            
        }

        [Theory]
        [InlineData("11111a11111",new [] {1,1,1,1,1,1,1,1,1,1,1})]
        public void TestarInterioInValido(string aindaNada,int[] valorInt)
        {
            Class1 cpf = new Class1();
            Assert.Throws<FormatException>(()=>cpf.InteiroValido(aindaNada));
           
        }

        //primeiro digito
        [Theory]
        [InlineData(new [] {1,1,1,4,4,4,7,7,7}, new [] {10,9,8,28,24,20,28,21,14})]
        public void TesteMultiplicaDigito1(int[] aindaNada, int [] resultado)
        {
            Class1 cpf = new Class1();
            int[] resultadosTeste = cpf.multiplicarValoresDigito1(aindaNada);
            Assert.Equal(resultado, resultadosTeste);
        
        }
        [Theory]
        [InlineData(162, 3)]
        public void TesteRestoDigito1(int aindaNada, int resto)
        {
            Class1 cpf = new Class1();
            int restoDeSoma = cpf.restoMultiplicacaoDigito1(aindaNada);
            Assert.Equal(restoDeSoma, resto); 
        
        }
        //segundo digito
        [Theory]
        [InlineData(new [] {1,1,1,4,4,4,7,7,7,3}, new [] {11,10,9,32,28,24,35,28,21,6})]
        public void TesteMultiplicaDigito2(int[] aindaNada, int [] resultado)
        {
            Class1 cpf = new Class1();
            int[] resultadosTeste = cpf.multiplicarValoresDigito2(aindaNada);
            Assert.Equal(resultadosTeste, resultado);
        
        }
        [Theory]
        [InlineData(204, 5)]
        public void TesteRestoDigito2(int aindaNada, int resto)
        {
            Class1 cpf = new Class1();
            int restoDeSoma = cpf.restoMultiplicacaoDigito2(aindaNada);
            Assert.Equal(restoDeSoma, resto); 
        }
        //validaçao geral
        [Theory]
        [InlineData("11144477735")]
        public void TesteValidarGeral(string aindaNada)
        {
            Class1 cpf = new Class1();
            string cpfValido = cpf.validarTudo(aindaNada);
            Assert.Equal(cpfValido, aindaNada);
        }
        [Theory]
        [InlineData("11144477715")]
        public void TesteValidarGeralInvalido(string aindaNada)
        {
            Class1 cpf = new Class1();
            string cpfValido = cpf.validarTudo(aindaNada);
            Assert.NotEqual(cpfValido, aindaNada);
        }

    }
}
